template |
** Cr model
cont
avs 10000 1e20
geo
liquid
concentrations
velocity
material
endavs
nobr
airw
1
20.  0.1
rich
0.95 1.e-5 1.e-4 1.e-4
zone
file
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/tet_material.zone
zonn
file # zone 81
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win1.zonn
zonn
file # zone 82
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win2.zonn
zonn
file # zone 83
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win3.zonn
perm
file
./h10_lowerTb4.perm
rock
file
./h10.rock
rlp
file
./h10.rlp
zonn 
file # zone 75
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_in.zonn
zonn
file # zone 76
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_out.zonn
zonn
file # zone 81
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win1.zonn
zonn
file # zone 82
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win2.zonn
zonn
file # zone 83
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win3.zonn
itfc
75  76  1.e-6


zone
file
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_abv.zone
pres
-1 0 0 0.09 0.1 2

pres
file
./pres_ss.macro
pres
file
./pres_sat.macro
pres
file
./pres_pwz.macro
# flow as boundary conditions on all lateral boundary nodes in the saturated zone
flow
file
./flow_sat.macro
flow
file
./flow_pwz.macro
zone 
file  # zone 1 for background infiltration
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/out_top.zone
zonn
file  # zone 62
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/TOP_ply_mortandad.zonn
zonn 
file  # zone 63
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/TOP_ply_sandia_w.zonn
zonn 
file  # zone 64
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/TOP_ply_lac.zonn
zonn   # zone 42
file
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_bot.zonn
zonn
file  # zone 13-18, pwz_bc
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/pwz_allsides.zonn
boun
model BG
ti
2 0 1e10
wgtz
dsw
|infil| |infil|
model Mortandad
ti 
2 0 1e10
wgtz
dsw  
|mc_flux| |mc_flux|
model Sandia 
ti 
2 0 1e10
wgtz
dsw  
|sc_flux| |sc_flux|
model LAC 
ti 
2 0 1e10
wgtz
dsw  
|lc_flux| |lc_flux|
model bottom  kz = 0 at the bottom
ti
2 0 1e10
kz
-20 -20 
model pwz_west
ti
2 0 1e10
wgtz
dsw
|west_flux| |west_flux|
end
-1  0 0 1
-62 0 0 2
-63 0 0 3
-64 0 0 4
-42 0 0 5
-13 0 0 6

zonn
file  # zone 43, west
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_west.zonn
zonn
file  # zone 44, south
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_south.zonn
zonn
file  # zone 45, east
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_east.zonn
zonn
file  # zone 46, north
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_north.zonn
zonn
file  # zone 47, north west
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_nw.zonn
zonn
file  # zone 48, south west
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_sw.zonn
zonn
file # zone 81
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win1.zonn
zonn
file # zone 82
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win2.zonn
zonn
file # zone 83
/scratch/nts/qkang/ep/cr/wc15c_proto/grid/basalt_win3.zonn
flxz
20
1 43 44 45 46 47 48 42 62 63 64 13 14 15 16 17 18 81 82 83
time
file
./time.macro
ctrl
15 1e-5 40 100 gmre
1 0 0 2

1.0 3 1.0
15  1.5  1.0000e-18 3.6525e8
0 1
sol
1 -1
iter
1.e-5 1.e-5 1.e-04 -1.e-2 1.2
0 0 0 0  1e20 
#zonn   # zone 42
#file
#/scratch/nts/qkang/ep/cr/wc15c_proto/grid/wtr_blw_bot.zonn
node
1
48591 
hist
years  100000  5.0
conc
end
trac
file
./trac.macro
stop
