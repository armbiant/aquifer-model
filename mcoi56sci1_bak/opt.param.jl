import Mads
p = DataStructures.OrderedDict("logk1"=>-15.5958,"logk2"=>-17.5619,"logk3"=>-16.5773,"logkbl"=>-12.3386,"logkbs"=>-12.2209,"logkab"=>-10.4713,"rbl"=>0.413815,"rbs"=>0.0115115,"rab"=>0.10405,"infil"=>-49.9521,"mc_flux"=>-78.7697,"sc_flux"=>-0.60579,"lc_flux"=>-0.146331,"west_flux"=>-39.9362,"crbc"=>-67.3316,"crst"=>14525.9,"cret"=>18221.3)
md = Mads.loadmadsfile("cr-v02.mads")
Mads.forward(md, p)
