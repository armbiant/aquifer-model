template |
** 3D UZ SZ model
cont
avs 500000 1e20
liquid
vapor
pressure
concentrations
saturation
velocity
material
endavs
restart
lflux
vflux

nobr
airw
1
20.  0.1
rich
0.95 1.e-5 1.e-4 1.e-4
zone
file
../grid/tet_material.zone
zonn
file # zone 81
../grid/basalt_win1.zonn
zonn
file # zone 82
../grid/basalt_win2.zonn
zonn
file # zone 83
../grid/basalt_win3.zonn
perm
file
./perm.dat
rock
file
./rock.dat
rlp
file
./rlp.dat
zonn 
file # zone 75
../grid/basalt_in.zonn
zonn
file # zone 76
../grid/basalt_out.zonn
zonn
file # zone 81
../grid/basalt_win1.zonn
zonn
file # zone 82
../grid/basalt_win2.zonn
zonn
file # zone 83
../grid/basalt_win3.zonn
itfc
75  76  1.e-6


zone
file
../grid/wtr_abv.zone
pres
-1 0 0 0.09 0.1 2

pres
file
../data/pres_ss.macro
pres
file
../setBoundConditions/pres_sat.macro
pres
file
../setBoundConditions/pres_pwz.macro
# flow as boundary conditions on all lateral boundary nodes in the saturated zone
flow
file
../setBoundConditions/flow_sat.macro
flow
file
../setBoundConditions/flow_pwz.macro
zone 
file  # zone 1 for background infiltration
../grid/out_top.zone
zonn
file  # zone 62
../grid/TOP_ply_mortandad.zonn
zonn 
file  # zone 63
../grid/TOP_ply_sandia_w.zonn
zonn 
file  # zone 64
../grid/TOP_ply_lac.zonn
zonn   # zone 42
file
../grid/wtr_blw_bot.zonn
zonn
file  # zone 13-18, pwz_bc
../grid/pwz_allsides.zonn
boun
model BG
ti
2 0 1e10
wgtz
dsw
|infil| |infil|
model Mortandad
ti 
2 0 1e10
wgtz
dsw  
|mc_flux| |mc_flux|
model Sandia 
ti 
2 0 1e10
wgtz
dsw  
|sc_flux| |sc_flux|
model LAC 
ti 
2 0 1e10
wgtz
dsw  
|lc_flux| |lc_flux|
model bottom  kz = 0 at the bottom
ti
2 0 1e10
kz
-20 -20 
model pwz_west
ti
2 0 1e10
wgtz
dsw
|west_flux| |west_flux|
end
-1  0 0 1
-62 0 0 2
-63 0 0 3
-64 0 0 4
-42 0 0 5
-13 0 0 6

zonn
file  # zone 43, west
../grid/wtr_blw_west.zonn
zonn
file  # zone 44, south
../grid/wtr_blw_south.zonn
zonn
file  # zone 45, east
../grid/wtr_blw_east.zonn
zonn
file  # zone 46, north
../grid/wtr_blw_north.zonn
zonn
file  # zone 47, north west
../grid/wtr_blw_nw.zonn
zonn
file  # zone 48, south west
../grid/wtr_blw_sw.zonn
zonn
file # zone 81
../grid/basalt_win1.zonn
zonn
file # zone 82
../grid/basalt_win2.zonn
zonn
file # zone 83
../grid/basalt_win3.zonn
flxz
20
1 43 44 45 46 47 48 42 62 63 64 13 14 15 16 17 18 81 82 83
time
1e-2 73050 1000000 001 2006 1 0

ctrl
15 1e-5 40 100 gmre
1 0 0 2

1.0 3 1.0
15  1.5  1.0000e-18 3.6525e8
0 1
sol
1 -1
iter
1.e-5 1.e-5 1.e-04 -1.e-2 1.2
0 0 0 0  1e20
stop
