#!/usr/bin/env bash

/n/swdev/FEHM_dev/STABLE/bin/xfehm_v3.3.0linUbuntu.18Apr16 flow.files 
../codes/post_process_avs_getpwz.x
cat ./simu_thickness.dat | awk '{print $3}' > thickness
tail -50 flow.outp > tempout1
grep '81 (    416)' tempout1 > tempout2
grep '82 (   1204)' tempout1 >> tempout2
grep '83 (    266)' tempout1 >> tempout2
cat tempout2 | awk '{print $6}' > winfluxes
/n/swdev/FEHM_dev/STABLE/bin/xfehm_v3.3.0linUbuntu.18Apr16 trans.files
../codes/read_brk.x
