!real*8,allocatable,dimension(:) :: con
!character(len=200) :: file1, file2
real*8 con,cmax,time,timax

!allocate(con(1:ne))

cmax=-1.e20
timax=0.
open(11,file='trans_h10_Cr.trc')
open(12,file='brkthr')
read(11,*)
read(11,*)
read(11,*)
read(11,*)
do while(1.eq.1)
  read(11,*,END=200) time,con 
  write(*,*) 'time, con=', time, con
  if(con > cmax) then
     cmax=con
     timax=time
  endif
enddo

200 write(*,*) 'timax,cmax=', timax, cmax
write(12,*) timax
close(11)
close(12)
end
