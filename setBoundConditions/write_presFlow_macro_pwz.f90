integer, allocatable, dimension(:) :: nodesA,  nodesLeft, nodes_col 
integer, allocatable, dimension(:) :: nodes_blw, nodes_blw1, nodes_lateral0
integer, allocatable, dimension(:,:) :: nodesB, nodes_lateral,nodes_eachside, nodes_eachside0
real*8, allocatable, dimension(:) :: x, y, z, z_wtr, thickness, thick, x_perch, y_perch
integer :: id_b(10), nnode_b(10)
real*8 :: highest_z, lowest_z, x0, y0

character(len=200):: gfile, blw_file, bfile(10), ctrl_file

nargc=iargc()
call getarg(1,ctrl_file)
if(nargc ==0) then
   write(*,*) 'missing argument'
   stop
endif

open(11,file=ctrl_file)
read(11,'(a)') gfile
read(11,'(a)') blw_file
read(11,*) nfile
do i=1,nfile
  read(11,'(a)') bfile(i)
  write(*,*) bfile(i)
enddo

open(12,file=gfile)
read(12,*) 
read(12,*) nnode
allocate(x(nnode), y(nnode), z(nnode))
do i=1,nnode
   read(12,*) itmp, x(i), y(i), z(i)
enddo
close(12)

! open the zone for all nodes below the water table (or in perched water zone)
! this file could be wtr.blw.zonn or *_in.zonn
open(13,file=blw_file)
read(13,*) 
read(13,*) 
read(13,*) 
read(13,*) n_blw
allocate(nodes_blw(n_blw))
read(13,*) (nodes_blw(i), i=1,n_blw)
close(13)

! for each node in this list, find the highest node from all saturated nodes (or perched nodes) that are above this node
! The highest location will be used to determine the hydraostatic head on these nodes.
allocate(z_wtr(n_blw), thickness(n_blw))

do i=1,n_blw
   itmp = nodes_blw(i)  ! node number
   x0 = x(itmp); y0=y(itmp); z0 = z(itmp)
   z_highest = -1.0d20
   z_lowest = 1.0d20
   do j=1,n_blw 
      jtmp = nodes_blw(j)
      if(x(jtmp) == x0 .and. y(jtmp) == y0) then
         if(z(jtmp) > z_highest) then
           z_highest =  z(jtmp)
           node_highest = jtmp
         endif
         if(z(jtmp) < z_lowest) then
           z_lowest =  z(jtmp)
           node_lowest = jtmp
         endif
      endif
   enddo
   thickness(i) = z(node_highest) - z(node_lowest)
   z_wtr(i) = z_highest
enddo

! now array thickness includes all nodes in the perched zone.
! check these nodes and keep one thickness value for any pair of (x,y), in other words, 
! For any column of perched zone nodes, keep one thickness value.
     allocate(x_perch(n_blw), y_perch(n_blw), thick(n_blw), nodes_blw1(n_blw))
     k = 1
     itmp = nodes_blw(1)  ! node number
     x_perch(1) = x(itmp); y_perch(1) = y(itmp) 
     thick(1) = thickness(1)
     nodes_blw1(1) = itmp
     do i=2,n_blw
          itmp = nodes_blw(i)  ! node number
          x0 = x(itmp); y0=y(itmp)
          do j=1,k
             jtmp = nodes_blw1(j)
             if(x(jtmp) == x0 .and. y(jtmp) == y0) then
               ! found a match so start looking again
                goto 90
             endif
         enddo
         ! no match found, so add it to the output
         k = k + 1
         x_perch(k) = x0; y_perch(k) = y0
         thick(k) = thickness(i)
         nodes_blw1(k) = itmp
90      continue
      enddo  
 nsize = k

! Now write out the thickness information for all nodes in saturated zone or perched water zone. 
! This informaiton will be used in comparison of the simulated perched zone and 'observed' zones.
if(ctrl_file(1:2) =='pw') then
   open(22,file='pwz_thickness.dat')
   write(22,*) nsize
   do i=1,nsize
      write(22,*)  x_perch(i),  y_perch(i),  thick(i)
   enddo
endif
close(22)

if(ctrl_file(1:2) =='pw') then
   open(22,file='pres_pwz.macro')
else
   open(22,file='pres_sat.macro')
endif
write(22,'(a4)') 'pres'
do i=1,n_blw
   itmp = nodes_blw(i)   ! this node
   wtr =  z_wtr(i)     ! elevation of the water table above this node
   diff = wtr - z(itmp) ! diff between this node and water table
!   write(23,*) i, itmp, wtr, z(itmp), diff
   diff = diff*9.8*997.80831/1.e6 + 0.1
   write(22,'(3i10,f12.6,f8.3,i3)') itmp, itmp, 1, diff, 1.0, 2
enddo
write(22,*)

! now read in nodes in all lateral boundaries and find those nodes that are also in
! saturated zone  (or perched zones)
allocate(nodes_lateral0(nnode), nodes_eachside0(nfile, 0:nnode))
nlateral0 = 0
!do ifile=1,nfile
do ifile=2,nfile
   iunit = 10 + ifile 
   open(iunit,file=bfile(ifile))
!  skip 3 lines
   read(iunit,*)
   read(iunit,*)
   read(iunit,*)

   read(iunit,*) itmp
   read(iunit,*) (nodes_eachside0(ifile, j),j=1,itmp)
   nodes_eachside0(ifile,0) = itmp
   nodes_lateral0(nlateral0+1:nlateral0+itmp) = nodes_eachside0(ifile, 1:itmp) ! also put in one array
   nlateral0 = nlateral0 + itmp
enddo

write(*,*) 'nlateral0 nodes_eachside0=', nlateral0, nodes_eachside0(:,0)

! now nlateral0 is the number of nodes on lateral boundary
! since these five zones shares the some nodes, better to remove the dupilicate nodes
! Actually, it will not affect the simulation results 

! check if these boundary nodes are in saturated/perched zone
nlateral = 0
allocate(nodes_lateral(2,nlateral0))
do i=1,nlateral0
   itmp = nodes_lateral0(i)
   do j=1,n_blw
      jtmp = nodes_blw(j)
      if(itmp == jtmp) then
         nlateral =  nlateral + 1
         nodes_lateral(1,nlateral) = jtmp  ! node number
         nodes_lateral(2,nlateral) = j     ! index in nodes_blw
      endif
   enddo
enddo

write(*,*) 'nlateral0 nlateral=', nlateral0, nlateral

! now write flow macro for boundary nodes
if(ctrl_file(1:2) == 'pw') then
   open(25,file='flow_pwz.macro')
else
   open(25,file='flow_sat.macro')
endif
write(25,'(a4)') 'flow'

   do i=1,nlateral
      itmp = nodes_lateral(1,i)    ! take the node number
      j = nodes_lateral(2,i)       ! index in nodes_blw
      diff = z_wtr(j) - z(itmp)    ! water table elevation and node elevation
      diff = diff*9.8*997.80831/1.e6 + 0.1 + 1.0e-3 ! in MPa unit
      write(25,'(3i10,f12.6,i3, e10.2)') itmp, itmp, 1, diff, 1, 1.e5
   enddo
write(25,*) 
close(25)

!  we need to write saturated/perched nodes for all sides

allocate(nodes_eachside(nfile,0:nnode))
do ifile=1,nfile
   ktmp = nodes_eachside0(ifile, 0)  ! number of total nodes on this boundary 
   do i=1, ktmp                      ! for each node on this boundary
      itmp = nodes_eachside0(ifile,i) ! the node number
      do j=1,n_blw                    ! loop over all saturated/perched nodes
         jtmp = nodes_blw(j)
         if(itmp == jtmp) then
            mtmp = nodes_eachside(ifile,0)    ! current number of nodes in this zone
            mtmp = mtmp + 1                   ! increment 
            nodes_eachside(ifile,0) = mtmp    ! update the number of nodes
            nodes_eachside(ifile,mtmp) = jtmp ! store this node number
         endif
      enddo
   enddo
enddo

! write zone for setting boun macro
    if(ctrl_file(1:2) == 'pw') then
       if(nfile ==1) then
          open(23,file='pwz_west.zonn')
       else
          open(23,file='pwz_allsides.zonn')
       endif
    else
       open(23,file='sat_allsides.zonn')
    endif
    write(23,'(a4)') 'zonn'

    do i=1,nfile
       write(23,*) i + 12
       write(23,'(a4)') 'nnum'
       itmp = nodes_eachside(i,0)
       write(23,*) itmp
       if(itmp >0) write(23,'(10i10)') (nodes_eachside(i,j), j=1,itmp)
    enddo
    write(23,*)
    close(23)
end


